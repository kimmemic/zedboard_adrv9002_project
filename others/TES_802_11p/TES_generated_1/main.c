/* This file contains code automatically generated by a machine. */
/* It has not been independently verified by any human. */
/* The generation process is deterministic and tested (not generative AI), */
/* but not every scenario or risk has been captured in unit tests. */
/* All code is provided as-is for example purposes only. */
/* The customer assumes all risks related to the use of this code. */


/* */
/* Silicon Revision: Presumed C0*/
/* */
/* Tx / Rx optimal carrier frequencies: 30 MHz to 6 GHz*/
/* External LO optimal frequencies: 60 MHz to 12 GHz*/
/* */
/* FPGA: v0.0.0*/
/* Device Driver API: v0.0.0*/
/* Device Driver Client: v68.10.1*/
/* Firmware: v0.22.27*/
/* Profile Generator: v0.53.2.0*/
/* Stream Generator Assembly: v0.7.10.0*/
/* Transceiver Evaluation Software: v0.25.0*/
/* ADRV9001 Plugin: v0.25.0*/

#include "initialize.h"
#include "calibrate.h"
#include "configure.h"
#include "prime.h"
#include "beginReceiving.h"
#include "dataCapture.h"
#include "stopReceiving.h"
#include "beginTransmitting.h"
#include "stopTransmitting.h"
#include <stdlib.h>
#include "linux_uio_init.h"


int main()
{
	int32_t error_code = 0;

	adi_adrv9001_Device_t * adrv9001Device_0 = (adi_adrv9001_Device_t *) calloc(1, sizeof(adi_adrv9001_Device_t));

	adi_fpga9001_Device_t * fpga9001Device_0 = (adi_fpga9001_Device_t *) calloc(1, sizeof(adi_fpga9001_Device_t));

	error_code = linux_uio_initialize(adrv9001Device_0, fpga9001Device_0, NULL, NULL, NULL, NULL, NULL);


	error_code = initialize(adrv9001Device_0, fpga9001Device_0);

	error_code = calibrate(adrv9001Device_0);

	error_code = configure(adrv9001Device_0);

	error_code = prime(adrv9001Device_0);

	error_code = beginReceiving(adrv9001Device_0, fpga9001Device_0);

	error_code = dataCapture(fpga9001Device_0);

	error_code = stopReceiving(fpga9001Device_0, adrv9001Device_0);

	error_code = beginTransmitting(adrv9001Device_0, fpga9001Device_0);

	error_code = stopTransmitting(fpga9001Device_0, adrv9001Device_0);


	return error_code;
}
