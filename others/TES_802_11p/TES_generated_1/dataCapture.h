/* This file contains code automatically generated by a machine. */
/* It has not been independently verified by any human. */
/* The generation process is deterministic and tested (not generative AI), */
/* but not every scenario or risk has been captured in unit tests. */
/* All code is provided as-is for example purposes only. */
/* The customer assumes all risks related to the use of this code. */


/* */
/* Silicon Revision: Presumed C0*/
/* */
/* Tx / Rx optimal carrier frequencies: 30 MHz to 6 GHz*/
/* External LO optimal frequencies: 60 MHz to 12 GHz*/
/* */
/* FPGA: v0.0.0*/
/* Device Driver API: v0.0.0*/
/* Device Driver Client: v68.10.1*/
/* Firmware: v0.22.27*/
/* Profile Generator: v0.53.2.0*/
/* Stream Generator Assembly: v0.7.10.0*/
/* Transceiver Evaluation Software: v0.25.0*/
/* ADRV9001 Plugin: v0.25.0*/

#ifndef _DATACAPTURE_H_
#define _DATACAPTURE_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "adi_fpga9001_datachain.h"
#include <stdlib.h>
#include "linux_uio_init.h"
extern int16_t dataCapture_iData_1[4096];
extern int16_t dataCapture_qData_1[4096];

int dataCapture(adi_fpga9001_Device_t * fpga9001Device_0);

#ifdef __cplusplus
}
#endif

#endif
