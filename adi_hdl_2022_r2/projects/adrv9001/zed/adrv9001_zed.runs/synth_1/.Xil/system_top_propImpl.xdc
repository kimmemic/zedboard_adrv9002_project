set_property SRC_FILE_INFO {cfile:c:/zedboard_adrv9002_project/adi_hdl_2022_r2/projects/adrv9001/zed/cmos_constr.xdc rfile:../../../cmos_constr.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:C:/zedboard_adrv9002_project/adi_hdl_2022_r2/projects/common/zed/zed_system_constr.xdc rfile:../../../../../common/zed/zed_system_constr.xdc id:2} [current_design]
set_property SRC_FILE_INFO {cfile:c:/zedboard_adrv9002_project/adi_hdl_2022_r2/projects/adrv9001/zed/system_constr.xdc rfile:../../../system_constr.xdc id:3 order:LATE} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN M20  IOSTANDARD LVCMOS18  }  [get_ports rx1_dclk_in_n]    ;## G07 FMC_HPC0_LA00_CC_N IO_L13N_T2_MRCC_34
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN M19  IOSTANDARD LVCMOS18  }  [get_ports rx1_dclk_in_p]    ;## G06 FMC_HPC0_LA00_CC_P IO_L13P_T2_MRCC_34
set_property src_info {type:XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN P22  IOSTANDARD LVCMOS18  }  [get_ports rx1_idata_in_n]   ;## G10 FMC_HPC0_LA03_N    IO_L16N_T2_34
set_property src_info {type:XDC file:1 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN N22  IOSTANDARD LVCMOS18  }  [get_ports rx1_idata_in_p]   ;## G09 FMC_HPC0_LA03_P    IO_L16P_T2_34
set_property src_info {type:XDC file:1 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN M22  IOSTANDARD LVCMOS18  }  [get_ports rx1_qdata_in_n]   ;## H11 FMC_HPC0_LA04_N    IO_L15N_T2_DQS_34
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN M21  IOSTANDARD LVCMOS18  }  [get_ports rx1_qdata_in_p]   ;## H10 FMC_HPC0_LA04_P    IO_L15P_T2_DQS_34
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN P18  IOSTANDARD LVCMOS18  }  [get_ports rx1_strobe_in_n]  ;## H08 FMC_HPC0_LA02_N    IO_L20N_T3_34
set_property src_info {type:XDC file:1 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN P17  IOSTANDARD LVCMOS18  }  [get_ports rx1_strobe_in_p]  ;## H07 FMC_HPC0_LA02_P    IO_L20P_T3_34
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN B20  IOSTANDARD LVCMOS18  }  [get_ports rx2_dclk_in_n]    ;## D21 FMC_HPC0_LA17_CC_N IO_L13N_T2_MRCC_35
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN B19  IOSTANDARD LVCMOS18  }  [get_ports rx2_dclk_in_p]    ;## D20 FMC_HPC0_LA17_CC_P IO_L13P_T2_MRCC_35
set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN G21  IOSTANDARD LVCMOS18  }  [get_ports rx2_idata_in_n]   ;## G22 FMC_HPC0_LA20_N    IO_L22N_T3_AD7N_35
set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN G20  IOSTANDARD LVCMOS18  }  [get_ports rx2_idata_in_p]   ;## G21 FMC_HPC0_LA20_P    IO_L22P_T3_AD7P_35
set_property src_info {type:XDC file:1 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN G16  IOSTANDARD LVCMOS18  }  [get_ports rx2_qdata_in_n]   ;## H23 FMC_HPC0_LA19_N    IO_L4N_T0_35
set_property src_info {type:XDC file:1 line:15 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN G15  IOSTANDARD LVCMOS18  }  [get_ports rx2_qdata_in_p]   ;## H22 FMC_HPC0_LA19_P    IO_L4P_T0_35
set_property src_info {type:XDC file:1 line:16 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN E20  IOSTANDARD LVCMOS18  }  [get_ports rx2_strobe_in_n]  ;## H26 FMC_HPC0_LA21_N    IO_L21N_T3_DQS_AD14N_35
set_property src_info {type:XDC file:1 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN E19  IOSTANDARD LVCMOS18  }  [get_ports rx2_strobe_in_p]  ;## H25 FMC_HPC0_LA21_P    IO_L21P_T3_DQS_AD14P_35
set_property src_info {type:XDC file:1 line:20 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN T17  IOSTANDARD LVCMOS18  }  [get_ports tx1_dclk_out_n]   ;## H14 FMC_HPC0_LA07_N    IO_L21N_T3_DQS_34
set_property src_info {type:XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN T16  IOSTANDARD LVCMOS18  }  [get_ports tx1_dclk_out_p]   ;## H13 FMC_HPC0_LA07_P    IO_L21P_T3_DQS_34
set_property src_info {type:XDC file:1 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN N20  IOSTANDARD LVCMOS18  }  [get_ports tx1_dclk_in_n]    ;## D09 FMC_HPC0_LA01_CC_N IO_L14N_T2_SRCC_34
set_property src_info {type:XDC file:1 line:23 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN N19  IOSTANDARD LVCMOS18  }  [get_ports tx1_dclk_in_p]    ;## D08 FMC_HPC0_LA01_CC_P IO_L14P_T2_SRCC_34
set_property src_info {type:XDC file:1 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J22  IOSTANDARD LVCMOS18  }  [get_ports tx1_idata_out_n]  ;## G13 FMC_HPC0_LA08_N    IO_L8N_T1_34
set_property src_info {type:XDC file:1 line:25 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J21  IOSTANDARD LVCMOS18  }  [get_ports tx1_idata_out_p]  ;## G12 FMC_HPC0_LA08_P    IO_L8P_T1_34
set_property src_info {type:XDC file:1 line:26 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN K18  IOSTANDARD LVCMOS18  }  [get_ports tx1_qdata_out_n]  ;## D12 FMC_HPC0_LA05_N    IO_L7N_T1_34
set_property src_info {type:XDC file:1 line:27 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J18  IOSTANDARD LVCMOS18  }  [get_ports tx1_qdata_out_p]  ;## D11 FMC_HPC0_LA05_P    IO_L7P_T1_34
set_property src_info {type:XDC file:1 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN L22  IOSTANDARD LVCMOS18  }  [get_ports tx1_strobe_out_n] ;## C11 FMC_HPC0_LA06_N    IO_L10N_T1_34
set_property src_info {type:XDC file:1 line:29 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN L21  IOSTANDARD LVCMOS18  }  [get_ports tx1_strobe_out_p] ;## C10 FMC_HPC0_LA06_P    IO_L10P_T1_34
set_property src_info {type:XDC file:1 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN F19  IOSTANDARD LVCMOS18  }  [get_ports tx2_dclk_out_n]   ;## G25 FMC_HPC0_LA22_N    IO_L20N_T3_AD6N_35
set_property src_info {type:XDC file:1 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN G19  IOSTANDARD LVCMOS18  }  [get_ports tx2_dclk_out_p]   ;## G24 FMC_HPC0_LA22_P    IO_L20P_T3_AD6P_35
set_property src_info {type:XDC file:1 line:33 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN C20  IOSTANDARD LVCMOS18  }  [get_ports tx2_dclk_in_n]    ;## C23 FMC_HPC0_LA18_CC_N IO_L14N_T2_AD4N_SRCC_35
set_property src_info {type:XDC file:1 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN D20  IOSTANDARD LVCMOS18  }  [get_ports tx2_dclk_in_p]    ;## C22 FMC_HPC0_LA18_CC_P IO_L14P_T2_AD4P_SRCC_35
set_property src_info {type:XDC file:1 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN D15  IOSTANDARD LVCMOS18  }  [get_ports tx2_idata_out_n]  ;## D24 FMC_HPC0_LA23_N    IO_L3N_T0_DQS_AD1N_35
set_property src_info {type:XDC file:1 line:36 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN E15  IOSTANDARD LVCMOS18  }  [get_ports tx2_idata_out_p]  ;## D23 FMC_HPC0_LA23_P    IO_L3P_T0_DQS_AD1P_35
set_property src_info {type:XDC file:1 line:37 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN C22  IOSTANDARD LVCMOS18  }  [get_ports tx2_qdata_out_n]  ;## G28 FMC_HPC0_LA25_N    IO_L16N_T2_35
set_property src_info {type:XDC file:1 line:38 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN D22  IOSTANDARD LVCMOS18  }  [get_ports tx2_qdata_out_p]  ;## G27 FMC_HPC0_LA25_P    IO_L16P_T2_35
set_property src_info {type:XDC file:1 line:39 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN A19  IOSTANDARD LVCMOS18  }  [get_ports tx2_strobe_out_n] ;## H29 FMC_HPC0_LA24_N    IO_L10N_T1_AD11N_35
set_property src_info {type:XDC file:1 line:40 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN A18  IOSTANDARD LVCMOS18  }  [get_ports tx2_strobe_out_p] ;## H28 FMC_HPC0_LA24_P    IO_L10P_T1_AD11P_35
set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  W18   IOSTANDARD LVCMOS33}           [get_ports hdmi_out_clk]
set_property src_info {type:XDC file:2 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  W17   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_vsync]
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  V17   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_hsync]
set_property src_info {type:XDC file:2 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U16   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data_e]
set_property src_info {type:XDC file:2 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y13   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[0]]
set_property src_info {type:XDC file:2 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA13  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[1]]
set_property src_info {type:XDC file:2 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA14  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[2]]
set_property src_info {type:XDC file:2 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y14   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[3]]
set_property src_info {type:XDC file:2 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB15  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[4]]
set_property src_info {type:XDC file:2 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB16  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[5]]
set_property src_info {type:XDC file:2 line:15 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA16  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[6]]
set_property src_info {type:XDC file:2 line:16 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB17  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[7]]
set_property src_info {type:XDC file:2 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA17  IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[8]]
set_property src_info {type:XDC file:2 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y15   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[9]]
set_property src_info {type:XDC file:2 line:19 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  W13   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[10]]
set_property src_info {type:XDC file:2 line:20 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  W15   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[11]]
set_property src_info {type:XDC file:2 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  V15   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[12]]
set_property src_info {type:XDC file:2 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U17   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[13]]
set_property src_info {type:XDC file:2 line:23 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  V14   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[14]]
set_property src_info {type:XDC file:2 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  V13   IOSTANDARD LVCMOS33  IOB TRUE} [get_ports hdmi_data[15]]
set_property src_info {type:XDC file:2 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U15   IOSTANDARD LVCMOS33} [get_ports spdif]
set_property src_info {type:XDC file:2 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB2   IOSTANDARD LVCMOS33} [get_ports i2s_mclk]
set_property src_info {type:XDC file:2 line:33 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA6   IOSTANDARD LVCMOS33} [get_ports i2s_bclk]
set_property src_info {type:XDC file:2 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y6    IOSTANDARD LVCMOS33} [get_ports i2s_lrclk]
set_property src_info {type:XDC file:2 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y8    IOSTANDARD LVCMOS33} [get_ports i2s_sdata_out]
set_property src_info {type:XDC file:2 line:36 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA7   IOSTANDARD LVCMOS33} [get_ports i2s_sdata_in]
set_property src_info {type:XDC file:2 line:40 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R7    IOSTANDARD LVCMOS33} [get_ports iic_scl]
set_property src_info {type:XDC file:2 line:41 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U7    IOSTANDARD LVCMOS33} [get_ports iic_sda]
set_property src_info {type:XDC file:2 line:42 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA18  IOSTANDARD LVCMOS33 PULLTYPE PULLUP} [get_ports iic_mux_scl[1]]
set_property src_info {type:XDC file:2 line:43 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y16   IOSTANDARD LVCMOS33 PULLTYPE PULLUP} [get_ports iic_mux_sda[1]]
set_property src_info {type:XDC file:2 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB4   IOSTANDARD LVCMOS33 PULLTYPE PULLUP} [get_ports iic_mux_scl[0]]
set_property src_info {type:XDC file:2 line:45 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB5   IOSTANDARD LVCMOS33 PULLTYPE PULLUP} [get_ports iic_mux_sda[0]]
set_property src_info {type:XDC file:2 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  L16   IOSTANDARD LVCMOS25} [get_ports otg_vbusoc]
set_property src_info {type:XDC file:2 line:53 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  P16   IOSTANDARD LVCMOS25} [get_ports gpio_bd[0]]       ; ## BTNC
set_property src_info {type:XDC file:2 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R16   IOSTANDARD LVCMOS25} [get_ports gpio_bd[1]]       ; ## BTND
set_property src_info {type:XDC file:2 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  N15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[2]]       ; ## BTNL
set_property src_info {type:XDC file:2 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R18   IOSTANDARD LVCMOS25} [get_ports gpio_bd[3]]       ; ## BTNR
set_property src_info {type:XDC file:2 line:57 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  T18   IOSTANDARD LVCMOS25} [get_ports gpio_bd[4]]       ; ## BTNU
set_property src_info {type:XDC file:2 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U10   IOSTANDARD LVCMOS33} [get_ports gpio_bd[5]]       ; ## OLED-DC
set_property src_info {type:XDC file:2 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U9    IOSTANDARD LVCMOS33} [get_ports gpio_bd[6]]       ; ## OLED-RES
set_property src_info {type:XDC file:2 line:60 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AB12  IOSTANDARD LVCMOS33} [get_ports gpio_bd[7]]       ; ## OLED-SCLK
set_property src_info {type:XDC file:2 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  AA12  IOSTANDARD LVCMOS33} [get_ports gpio_bd[8]]       ; ## OLED-SDIN
set_property src_info {type:XDC file:2 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U11   IOSTANDARD LVCMOS33} [get_ports gpio_bd[9]]       ; ## OLED-VBAT
set_property src_info {type:XDC file:2 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U12   IOSTANDARD LVCMOS33} [get_ports gpio_bd[10]]      ; ## OLED-VDD
set_property src_info {type:XDC file:2 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  F22   IOSTANDARD LVCMOS25} [get_ports gpio_bd[11]]      ; ## SW0
set_property src_info {type:XDC file:2 line:66 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  G22   IOSTANDARD LVCMOS25} [get_ports gpio_bd[12]]      ; ## SW1
set_property src_info {type:XDC file:2 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H22   IOSTANDARD LVCMOS25} [get_ports gpio_bd[13]]      ; ## SW2
set_property src_info {type:XDC file:2 line:68 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  F21   IOSTANDARD LVCMOS25} [get_ports gpio_bd[14]]      ; ## SW3
set_property src_info {type:XDC file:2 line:69 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H19   IOSTANDARD LVCMOS25} [get_ports gpio_bd[15]]      ; ## SW4
set_property src_info {type:XDC file:2 line:70 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H18   IOSTANDARD LVCMOS25} [get_ports gpio_bd[16]]      ; ## SW5
set_property src_info {type:XDC file:2 line:71 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H17   IOSTANDARD LVCMOS25} [get_ports gpio_bd[17]]      ; ## SW6
set_property src_info {type:XDC file:2 line:72 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  M15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[18]]      ; ## SW7
set_property src_info {type:XDC file:2 line:74 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  T22   IOSTANDARD LVCMOS33} [get_ports gpio_bd[19]]      ; ## LD0
set_property src_info {type:XDC file:2 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  T21   IOSTANDARD LVCMOS33} [get_ports gpio_bd[20]]      ; ## LD1
set_property src_info {type:XDC file:2 line:76 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U22   IOSTANDARD LVCMOS33} [get_ports gpio_bd[21]]      ; ## LD2
set_property src_info {type:XDC file:2 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U21   IOSTANDARD LVCMOS33} [get_ports gpio_bd[22]]      ; ## LD3
set_property src_info {type:XDC file:2 line:78 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  V22   IOSTANDARD LVCMOS33} [get_ports gpio_bd[23]]      ; ## LD4
set_property src_info {type:XDC file:2 line:79 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  W22   IOSTANDARD LVCMOS33} [get_ports gpio_bd[24]]      ; ## LD5
set_property src_info {type:XDC file:2 line:80 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U19   IOSTANDARD LVCMOS33} [get_ports gpio_bd[25]]      ; ## LD6
set_property src_info {type:XDC file:2 line:81 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  U14   IOSTANDARD LVCMOS33} [get_ports gpio_bd[26]]      ; ## LD7
set_property src_info {type:XDC file:2 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[27]]      ; ## XADC-GIO0
set_property src_info {type:XDC file:2 line:84 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[28]]      ; ## XADC-GIO1
set_property src_info {type:XDC file:2 line:85 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  K15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[29]]      ; ## XADC-GIO2
set_property src_info {type:XDC file:2 line:86 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  J15   IOSTANDARD LVCMOS25} [get_ports gpio_bd[30]]      ; ## XADC-GIO3
set_property src_info {type:XDC file:2 line:88 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  G17   IOSTANDARD LVCMOS25} [get_ports gpio_bd[31]]      ; ## OTG-RESETN
set_property src_info {type:XDC file:2 line:91 export:INPUT save:INPUT read:READ} [current_design]
create_clock -name spi0_clk      -period 40   [get_pins -hier */EMIOSPI0SCLKO]
set_property src_info {type:XDC file:2 line:92 export:INPUT save:INPUT read:READ} [current_design]
create_clock -name spi1_clk      -period 40   [get_pins -hier */EMIOSPI1SCLKO]
set_property src_info {type:XDC file:3 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN L18  IOSTANDARD LVCMOS18}  [get_ports dev_clk_out]       ; # H04 FMC_HPC0_CLK0_M2C_P  IO_L12P_T1_MRCC_34
set_property src_info {type:XDC file:3 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J20  IOSTANDARD LVCMOS18}  [get_ports dgpio_0]           ; # G18 FMC_HPC0_LA16_P      IO_L9P_T1_DQS_34
set_property src_info {type:XDC file:3 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN K21  IOSTANDARD LVCMOS18}  [get_ports dgpio_1]           ; # G19 FMC_HPC0_LA16_N      IO_L9N_T1_DQS_34
set_property src_info {type:XDC file:3 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J17  IOSTANDARD LVCMOS18}  [get_ports dgpio_2]           ; # H20 FMC_HPC0_LA15_N      IO_L2N_T0_34
set_property src_info {type:XDC file:3 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN N18  IOSTANDARD LVCMOS18}  [get_ports dgpio_3]           ; # H17 FMC_HPC0_LA11_N      IO_L5N_T0_34
set_property src_info {type:XDC file:3 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN R21  IOSTANDARD LVCMOS18}  [get_ports dgpio_4]           ; # D15 FMC_HPC0_LA09_N      IO_L17N_T2_34
set_property src_info {type:XDC file:3 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN T19  IOSTANDARD LVCMOS18}  [get_ports dgpio_5]           ; # C15 FMC_HPC0_LA10_N      IO_L22N_T3_34
set_property src_info {type:XDC file:3 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN E21  IOSTANDARD LVCMOS18}  [get_ports dgpio_6]           ; # C26 FMC_HPC0_LA27_P      IO_L17P_T2_AD5P_35
set_property src_info {type:XDC file:3 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN F18  IOSTANDARD LVCMOS18}  [get_ports dgpio_7]           ; # D26 FMC_HPC0_LA26_P      IO_L5P_T0_AD9P_35
set_property src_info {type:XDC file:3 line:15 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN A16  IOSTANDARD LVCMOS18}  [get_ports dgpio_8]           ; # H31 FMC_HPC0_LA28_P      IO_L9P_T1_DQS_AD3P_35
set_property src_info {type:XDC file:3 line:16 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN A17  IOSTANDARD LVCMOS18}  [get_ports dgpio_9]           ; # H32 FMC_HPC0_LA28_N      IO_L9N_T1_DQS_AD3N_35
set_property src_info {type:XDC file:3 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN N17  IOSTANDARD LVCMOS18}  [get_ports dgpio_10]          ; # H16 FMC_HPC0_LA11_P      IO_L5P_T0_34
set_property src_info {type:XDC file:3 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN D21  IOSTANDARD LVCMOS18}  [get_ports dgpio_11]          ; # C27 FMC_HPC0_LA27_N      IO_L17N_T2_AD5N_35
set_property src_info {type:XDC file:3 line:20 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN C15  IOSTANDARD LVCMOS18}  [get_ports gp_int]            ; # H34 FMC_HPC0_LA30_P      IO_L7P_T1_AD2P_35
set_property src_info {type:XDC file:3 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN L17  IOSTANDARD LVCMOS18}  [get_ports mode]              ; # D17 FMC_HPC0_LA13_P      IO_L4P_T0_34
set_property src_info {type:XDC file:3 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN M17  IOSTANDARD LVCMOS18}  [get_ports reset_trx]         ; # D18 FMC_HPC0_LA13_N      IO_L4N_T0_34
set_property src_info {type:XDC file:3 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN R19  IOSTANDARD LVCMOS18}  [get_ports rx1_enable]        ; # C14 FMC_HPC0_LA10_P      IO_L22P_T3_34
set_property src_info {type:XDC file:3 line:25 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN E18  IOSTANDARD LVCMOS18}  [get_ports rx2_enable]        ; # D27 FMC_HPC0_LA26_N      IO_L5N_T0_AD9N_35
set_property src_info {type:XDC file:3 line:27 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN L19  IOSTANDARD LVCMOS18}  [get_ports sm_fan_tach]       ; # H05 FMC_HPC0_CLK0_M2C_N  IO_L12N_T1_MRCC_34
set_property src_info {type:XDC file:3 line:29 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN P20  IOSTANDARD LVCMOS18}  [get_ports spi_clk]           ; # G15 FMC_HPC0_LA12_P      IO_L18P_T2_34
set_property src_info {type:XDC file:3 line:30 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN C18  IOSTANDARD LVCMOS18}  [get_ports spi_dio]           ; # G31 FMC_HPC0_LA29_N      IO_L11N_T1_SRCC_35
set_property src_info {type:XDC file:3 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN P21  IOSTANDARD LVCMOS18}  [get_ports spi_do]            ; # G16 FMC_HPC0_LA12_N      IO_L18N_T2_34
set_property src_info {type:XDC file:3 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN J16  IOSTANDARD LVCMOS18}  [get_ports spi_en]            ; # H19 FMC_HPC0_LA15_P      IO_L2P_T0_34
set_property src_info {type:XDC file:3 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN R20  IOSTANDARD LVCMOS18}  [get_ports tx1_enable]        ; # D14 FMC_HPC0_LA09_P      IO_L17P_T2_34
set_property src_info {type:XDC file:3 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN C17  IOSTANDARD LVCMOS18}  [get_ports tx2_enable]        ; # G30 FMC_HPC0_LA29_P      IO_L11P_T1_SRCC_35
set_property src_info {type:XDC file:3 line:37 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN B16  IOSTANDARD LVCMOS18}  [get_ports vadj_err]          ; # G33 FMC_HPC0_LA31_P      IO_L8P_T1_AD10P_35
set_property src_info {type:XDC file:3 line:38 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN B17  IOSTANDARD LVCMOS18}  [get_ports platform_status]   ; # G34 FMC_HPC0_LA31_N      IO_L8N_T1_AD10N_35
set_property src_info {type:XDC file:3 line:41 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  L16   IOSTANDARD LVCMOS18} [get_ports otg_vbusoc]
set_property src_info {type:XDC file:3 line:42 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  P16   IOSTANDARD LVCMOS18} [get_ports gpio_bd[0]]       ; ## BTNC
set_property src_info {type:XDC file:3 line:43 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R16   IOSTANDARD LVCMOS18} [get_ports gpio_bd[1]]       ; ## BTND
set_property src_info {type:XDC file:3 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  N15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[2]]       ; ## BTNL
set_property src_info {type:XDC file:3 line:45 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R18   IOSTANDARD LVCMOS18} [get_ports gpio_bd[3]]       ; ## BTNR
set_property src_info {type:XDC file:3 line:46 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  T18   IOSTANDARD LVCMOS18} [get_ports gpio_bd[4]]       ; ## BTNU
set_property src_info {type:XDC file:3 line:47 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  F22   IOSTANDARD LVCMOS18} [get_ports gpio_bd[11]]      ; ## SW0
set_property src_info {type:XDC file:3 line:48 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  G22   IOSTANDARD LVCMOS18} [get_ports gpio_bd[12]]      ; ## SW1
set_property src_info {type:XDC file:3 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H22   IOSTANDARD LVCMOS18} [get_ports gpio_bd[13]]      ; ## SW2
set_property src_info {type:XDC file:3 line:50 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  F21   IOSTANDARD LVCMOS18} [get_ports gpio_bd[14]]      ; ## SW3
set_property src_info {type:XDC file:3 line:51 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H19   IOSTANDARD LVCMOS18} [get_ports gpio_bd[15]]      ; ## SW4
set_property src_info {type:XDC file:3 line:52 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H18   IOSTANDARD LVCMOS18} [get_ports gpio_bd[16]]      ; ## SW5
set_property src_info {type:XDC file:3 line:53 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H17   IOSTANDARD LVCMOS18} [get_ports gpio_bd[17]]      ; ## SW6
set_property src_info {type:XDC file:3 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  M15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[18]]      ; ## SW7
set_property src_info {type:XDC file:3 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  H15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[27]]      ; ## XADC-GIO0
set_property src_info {type:XDC file:3 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  R15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[28]]      ; ## XADC-GIO1
set_property src_info {type:XDC file:3 line:57 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  K15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[29]]      ; ## XADC-GIO2
set_property src_info {type:XDC file:3 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  J15   IOSTANDARD LVCMOS18} [get_ports gpio_bd[30]]      ; ## XADC-GIO3
set_property src_info {type:XDC file:3 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  G17   IOSTANDARD LVCMOS18} [get_ports gpio_bd[31]]      ; ## OTG-RESETN
set_property src_info {type:XDC file:3 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_property  -dict {PACKAGE_PIN  Y11   IOSTANDARD LVCMOS33} [get_ports  tdd_sync]        ; ## JA1.JA1
